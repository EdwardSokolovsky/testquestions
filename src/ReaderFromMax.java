import java.io.*;
import java.util.*;

public class ReaderFromMax {
    public static void main(String[] args) throws IOException {
        //используем класс BufferedReader для чтения из файла
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        //объявляем переменную типа File:
        File f = new File("src\\arr.txt");
        //объявляем вторую переменную f2 передавая в неё экземмпляр f:
        BufferedReader f2 = new BufferedReader(new FileReader(f));
        //передаем считанную информацию в строковую переменную line:
        String line = f2.readLine();
        //преобразуем считаную строку в список типа String:
        List<String> arr = new ArrayList<String> (Arrays.asList(line.split(",")));
        //добавляем в новый лист типа Integer, элементы списка arr, при помощи цикла for:
        List<Integer> integerArr = new ArrayList<>();
        for (String s : arr)
            integerArr.add(Integer.parseInt(s));
        //осуществляем сортировку реверсом (от большего к меньшему):
        Collections.reverse(integerArr);
        //выводим в консоль:
        System.out.println(integerArr);

        }
    }