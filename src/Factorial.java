public class Factorial {

    //главный метод:

    public static void main(String[] args) {

        //выводим наш результат в консоль:
        System.out.println("20! = " + factorial(20));


    }
/*  Функция для вычесления, на месте x будет число, исходя из задания: 20
    пусть наш результат будет типа long на всякий пожарный, мало ли, какое там число может оказаться на месте 20):*/
    public static long factorial(long x){
        //задаем начальный result:
        long result = 1;
/*        будем пробигаться по циклу, начиная с 2, так, 2*1 => result =2; далее i = 3: result(2)*3=6;
        далее result(6)*3=18 итд до i = 20 включительно.*/
        for (int i=2; i<=x; i++){
            //формула для result (final result = result after cicle * currrent i)
            result *= i;

        }
        //возвращаем наш конечный результат:
        return result;
    }


}
